const productcontroller=require("../controller/product.controller");
const express = require("express");
const router =express.Router();
router.post("/",productcontroller.create);
router.delete("/",productcontroller.delete);
router.put("/",productcontroller.update);
router.get("/",productcontroller.find);
router.get("/:id",productcontroller.findById);
module.exports =router;