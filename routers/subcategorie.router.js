const subcategorycontroller=require("../controller/subcategorie.controller");
const express = require("express");
const router =express.Router();
router.post("/",subcategorycontroller.create);
router.delete("/",subcategorycontroller.delete);
router.put("/",subcategorycontroller.update);
router.get("/",subcategorycontroller.find);
router.get("/:id",subcategorycontroller.findById);
module.exports =router;