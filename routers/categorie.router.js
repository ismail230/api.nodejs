const categorycontroller =require("../controller/categorie.controller");
const express = require("express");
const router =express.Router();
router.post("/",categorycontroller.create);
router.delete("/",categorycontroller.delete);
router.put("/",categorycontroller.update);
router.get("/",categorycontroller.find);
router.get("/:id",categorycontroller.findById);
module.exports =router;