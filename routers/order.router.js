const ordercontroller=require("../controller/order.controller");
const express = require("express");
const router =express.Router();
router.post("/",ordercontroller.create);
router.delete("/",ordercontroller.delete);
router.put("/",ordercontroller.update);
router.get("/",ordercontroller.find);
router.get("/:id",ordercontroller.findById);
module.exports =router;