const mongoose = require("mongoose");
const bcrypt =require("bcrypt");
const Schema = mongoose.Schema;
const UserScheme = new Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  orders :[{type:Schema.Types.ObjectId, ref:"Orders"}] 
});

// hash user password before saving into database
UserScheme.pre('save', function(next){
  this.password = bcrypt.hashSync(this.password, 10);
  next();
  });
   


module.exports=mongoose.model("Users",UserScheme);