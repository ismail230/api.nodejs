const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ProductScheme = new Schema({
  reference: String,
  color: String,
  price:String,
  description :String,
  Subcategory: {type : Schema.Types.ObjectId ,ref:"Subcategories" },
  orders : [{type:Schema.Types.ObjectId, ref:"Orders"}]
});
module.exports=mongoose.model("¨Products",ProductScheme);