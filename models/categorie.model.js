const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const CategorieScheme = new Schema({
  description: String,
  name: String,
  reference: String,
  Subcategories : [{type : Schema.Types.ObjectId, ref:"Subcategory"}] 
});
module.exports=mongoose.model("Categories",CategorieScheme);