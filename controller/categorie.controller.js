const categorieModel = require("../models/categorie.model");
module.exports = {  
    
    create:function(req,res,next){
        console.log(req.body) 
        const category=new categorieModel(req.body);
        category.save(req.body, function (err, result) {
            if (err) 
             next(err);
            else
            result.populate("Subcategories").execpopulate(function(err,item){ 
            res.json({status: "success", message: " categorieModel added successfully!!!", data: result});
            
          });
        });
        },
    update:function(req,res,next){
        console.log(req.body) 
        categorieModel.findOneAndUpdate({_id:req.body.id},req.body,{new:true} ,function (err, result) {
            if (err) 
             next(err);
            else
             res.json({status: "success", message: " categorieModel updated successfully!!!", data: null});

          });
    },
    delete:function(req,res,next){
        console.log(req.body) 
        categorieModel.findOneAndDelete({_id:req.body.id},req.body, function (err, result) {
            if (err) 
             next(err);
            else
             res.json({status: "success", message: " categorieModel deleted successfully!!!", data: null});

          });
    },
    find:function(req,res,next){
        console.log(req.body) 
        categorieModel.find({}).populate("subcategory").exec( function (err, result) {
            if (err) 
             next(err);
            else
             res.json({status: "success", message: " categorieModel found successfully!!!", data: result});

          });
    },
    findById:function(req,res,next){
        {
            console.log(req.body) 
            categorieModel.find({_id:req.params.id}).populate("user").exec( function (err, result) {
                if (err) 
                 next(err);
                else
                 res.json({status: "success", message: " categorieModel foundid successfully!!!", data: result});
    
              });
        }
    }

}