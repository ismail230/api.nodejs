const subcategorieModel = require("../models/subcategorie.model");
module.exports = {  
    
    create:function(req,res,next) 
    {
        console.log(req.body) 
        const subcategory=new subcategorieModel(req.body);
        subcategory.save( function (err, result) {
            if (err) 
             next(err);
            else
            result.populate("category").populate("product").execPopulate(function(err,item){
            res.json({status: "success", message: "Subcategory added successfully!!!", data: result});
            
          });
        });

    },
    update:function(req,res,next) 
    {
        console.log(req.body) 
        subcategorieModel.findOneAndUpdate({_id:req.body.id},req.body,{new:true} ,function (err, result) {
            if (err) 
             next(err);
            else 

             res.json({status: "success", message: "subcategory updated successfully!!!", data: null});

          });
    },
    delete:function(req,res,next) 
    {
        console.log(req.body) 
        subcategorieModel.findOneAndDelete({_id:req.body.id},req.body, function (err, result) {
            if (err) 
             next(err);
            else
             res.json({status: "success", message: "Subcategory deleted successfully!!!", data: null});

          });
    },
    find:function(req,res,next) 
    {
        console.log(req.body) 
        subcategorieModel.find({}).populate("category").populate("product").exec( function (err, result) {
            if (err) 
             next(err);
            else
             res.json({status: "success", message: "Subcategory found successfully!!!", data: result});

          });
    },
    findById:function(req,res,next) {
        {
            console.log(req.body) 
            subcategorieModel.find({_id:req.params.id}).populate("category").populate("product").exec( function (err, result) {
                if (err) 
                 next(err);
                else
                 res.json({status: "success", message: "Subcategory foundid successfully!!!", data: result});
    
              });
        }
    }  
  }

