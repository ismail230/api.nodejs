const userModel = require("../models/user.model");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { populate } = require("../models/user.model");
module.exports = {

    create: function (req, res, next) {
        console.log(req.body)
        userModel.create(req.body, function (err, result) {
            if (err)
                next(err);
            else
                res.json({ status: "success", message: "User added successfully!!!", data: null });

        });
    },


    update: function (req, res, next) {
        console.log(req.body)
        userModel.findOneAndUpdate({ _id: req.body.id }, req.body, { new: true }, function (err, result) {
            if (err)
                next(err);
            else
                res.json({ status: "success", message: "User updated successfully!!!", data: null });

        });
    },


    delete: function (req, res, next) {
        console.log(req.body)
        userModel.findOneAndDelete({ _id: req.body.id }, req.body, function (err, result) {
            if (err)
                next(err);
            else
                res.json({ status: "success", message: "User deleted successfully!!!", data: null });

        });
    },
    login: function (req, res, next) {
        console.log(req.body)
        userModel.findOne({ email: req.body.email }, function (err, userInfo) {
            if (err) {
                next(err);
            } else {
                console.log(userInfo)
                if (bcrypt.compareSync(req.body.password, userInfo.password)) {
                    const token = jwt.sign({ id: userInfo._id }, req.app.get('secretKey'), { expiresIn: '1h' });
                    res.json({ status: "success", message: "user found!!!", data: { user: userInfo, token: token } });
                } else {
                    res.json({ status: "error", message: "Invalid email/password!!!", data: null });
                }
            }
        });
    },

    find: function(req, res, next) {
        console.log(req.body)
        userModel.find({}).populate("order").exec( function (err, result) {
            if (err)
                next(err);
            else
                res.json({ status: "success", message: "User found successfully!!!", data: result });

        });

    },
findById: function(req, res, next) {
    console.log(req.body)
    const user=new userModel(req.body);
    user.save (function (err, result) {
        if (err)
            next(err);
        else
        result.populate("Orders").execPopulate(function(err,item){
        res.json({ status: "success", message: "User foundid successfully!!!", data: result });

    });
});
}

}
